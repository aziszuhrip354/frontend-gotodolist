<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="public/style.css">

    <title>GO TODOLIST</title>
    <style>
      body {
        background: rgb(205,203,242);
        background: linear-gradient(90deg, rgba(205,203,242,1) 0%, rgba(255,212,226,1) 51%);
      }
    </style>
  </head>
  <body>
    
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-md-12 font-weight-bold">
                <p class="text-center">
                    GO TODOLIST
                </p>
            </div>
            <div class="col-md-12 text-center">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalAddTitle">
                    Add Todo
                </button>
            </div>
        </div>
    </div>            
    
    <div class="container-fluid">
      <div class="row" id="containerTitle">
          <!-- <div class="col-2">
            <div class="glass p-2 h-100">
                <span >test</span>
              </div>
              <div class="glass">
                <a class="btn btn-primary w-100" href="#">Detail</a>
              </div>
          </div> -->
      </div>
    </div>

  
  <!-- Modal -->
  <div class="modal fade" id="modalAddTitle" role="dialog"  aria-labelledby="modalAddTitleLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalAddTitleLabel">Add Title Todo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="post" id="addTodo">
                <div class="form-group">
                  <label for="exampleInputEmail1">Title</label>
                  <input type="text" class="form-control" name="todo_title">
                </div>
              </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="addTodoList();">Save</button>
        </div>
      </div>
    </div>
  </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
      // Swal.fire({
      //   title: 'Are you sure?',
      //   text: "You won't be able to revert this!",
      //   icon: 'success',
      //   showCancelButton: false,
      //   showConfirmButton: false,
      //   confirmButtonColor: '#3085d6',
      //   timer: 2000,
      // })
        const getAllTitle = async () => {
          $("#containerTitle").html("");
          await fetch('http://localhost:3000/v1/title-todo/data/all')
                .then(response => response.json())
                .then(data => {
                  let dataJson = data.data;
                  dataJson.forEach(val => {
                          let template = `<div class="col-2">
                                            <div class="glass p-2 h-100">
                                                <span >${val.todo_title}</span>
                                              </div>
                                              <div class="glass">
                                                <a class="btn btn-primary w-100" href="#">Detail</a>
                                              </div>
                                          </div>`
                          $("#containerTitle").append(template)
                  });
                })
                .catch(err =>  {
                  console.log(err);
                  Swal.fire({
                    title: 'Message',
                    text: `Connection refused!: ${err}`,
                    icon: 'error',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    timer: 2000,
                  })
                });
        }
        getAllTitle();
        
        const endpoint = "http://localhost:3000";

        const addTodoList = () => {
            let todo = $("[name='todo_title']").val();
            let url = `${endpoint}/v1/title-todo/data`
            $.ajax({
                url: url,
                dataType: "JSON",
                type: "POST",
                async: true,
                data: JSON.stringify( {
                    todo_title: todo
                }),
                success: function (data) {
                    if(data.status) {
                      Swal.fire({
                            title: 'Message:',
                            text: data.message,
                            icon: 'success',
                            showCancelButton: false,
                            showConfirmButton: false,
                            confirmButtonColor: '#3085d6',
                            timer: 2000,
                      })
                      getAllTitle();
                      $("[name='todo_title']").val("");
                      $("#modalAddTitle").hide()
                      $('.modal-backdrop').remove();
                    } else {
                         Swal.fire({
                            title: 'Message:',
                            text: data.message,
                            icon: 'error',
                            showCancelButton: false,
                            showConfirmButton: false,
                            confirmButtonColor: '#3085d6',
                            timer: 2000,
                        })
                    }
                },
                error: function (xhr, exception) {
                   alert("Something error....");
                }
            }); 
        }
    </script>
  </body>
</html>
