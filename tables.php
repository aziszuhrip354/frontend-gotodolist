<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="assets/img/favicon.png">
  <title>
    Go Todolist Apps
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link id="pagestyle" href="assets/css/argon-dashboard.css?v=2.0.4" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
  <link rel="stylesheet" href="public/style.css">
  <style>
    .navbar-vertical.navbar-expand-xs .navbar-nav .nav-link {
     padding-top: inherit!important;
     padding-bottom: inherit!important;
     margin: 0 0.5rem;
}
.navbar-vertical .navbar-nav .nav-link {
    padding-left: inherit!important;
    padding-right: inherit!important;
    font-weight: 500;
    color: #67748e;
}

.table tbody tr:last-child td {
    border-width: initial!important;
}
  </style>
</head>

<body class="g-sidenav-show   bg-gray-100">
  <div class="min-height-300 bg-primary position-absolute w-100"></div>
  <aside class="overflow-hidden sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 " id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href=" https://demos.creative-tim.com/argon-dashboard/pages/dashboard.html " target="_blank">
        <img src="assets/img/logo-ct-dark.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold">Go Todolist Apps</span>
      </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse vh-100 " id="sidenav-collapse-main">
      <ul class="navbar-nav overflow-hidden">
        <div id="accordion">
          <li class="nav-item h-50">
              <a class="nav-link active hp-100" href="#"  data-toggle="modal" data-target="#modalAddTitle">
                <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                  <i class="ni ni-calendar-grid-58 text-danger text-sm opacity-10"></i>
                </div>
                <span class="nav-link-text ms-1">Add Todo Title</span>
              </a>
            </li>
          <li class="nav-item h-50" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <a class="nav-link active h-50" href="#">
              <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                <i class="ni ni-calendar-grid-58 text-warning text-sm opacity-10"></i>
              </div>
              <span class="nav-link-text ms-1 ">Todolist Title</span>
            </a>
          </li>
          <div id="collapseOne" class="collapse card show overflow-auto" aria-labelledby="headingOne" data-parent="#accordion">
            <!-- <li class="nav-item pl-2">
              <a class="nav-link " href="../pages/sign-up.html">
                <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                  <i class="ni ni-collection text-info text-sm opacity-10"></i>
                </div>
                <span class="nav-link-text ms-1">Sign Up</span>
              </a>
            </li> -->
          </div>
        </div>
      </ul>
    </div>
  </aside>
  <main class="main-content position-relative border-radius-lg " id="containerTodo">
    <!-- Navbar -->
    <!-- End Navbar -->
    <!-- <div class="container-fluid py-4 vh-95">
      <div class="row hp-100">
          <div class="col-md-12 mt-4 hp-100">
            <div class="card hp-100">
              <div class="card-header pb-0 px-3 d-flex justify-content-between align-items-center">
                <h6 class="my-2">Todolist: Test</h6>
                <button class="btn btn-outline-primary btn-sm mb-2" data-toggle="modal" data-target="#modalAddTodo">Add New Todo</button>
              </div>
              <span class="ml-2 text-center">Total Todo : 0</span>
              <div class="card-body pt-4 p-3 hp-100 bg-gradient">
                <ul class="list-group overflow-auto hp-100">
                  <li class="list-group-item border-0 d-flex p-4 mb-2 glass border-radius-lg">
                    <div class="d-flex flex-column">
                      <h4 class="mb-3 t-primary">Oliver Liam</h4>
                      <span class="mb-2 text-xs">Company Name: <span class="text-dark font-weight-bold ms-sm-2">Viking Burrito</span></span>
                      <span class="mb-2 text-xs">Email Address: <span class="text-dark ms-sm-2 font-weight-bold">oliver@burrito.com</span></span>
                      <span class="text-xs">VAT Number: <span class="text-dark ms-sm-2 font-weight-bold">FRB1235476</span></span>
                    </div>
                    <div class="ms-auto text-end">
                      <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="javascript:;"><i class="far fa-trash-alt me-2" aria-hidden="true"></i>Delete</a>
                      <a class="btn btn-link text-dark px-3 mb-0" href="javascript:;"><i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer pt-4 mt-2">
          <div class="container-fluid">
            <div class="row align-items-center justify-content-lg-between">
              <div class="col-lg-12 mb-lg-0 mb-4 text-center">
                <div class="copyright text-center text-sm text-muted text-lg-start">
                  © <script>
                    document.write(new Date().getFullYear())
                  </script>,
                  made with <i class="fa fa-heart"></i> by
                  <a href="https://www.creative-tim.com" class="font-weight-bold" target="_blank">Azis Zuhri Pratomo</a>
                </div>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div> -->
  </main>
  <div class="fixed-plugin">
    <a class="fixed-plugin-button text-dark position-fixed px-3 py-2">
      <i class="fa fa-cog py-2"> </i>
    </a>
    <div class="card shadow-lg">
      <div class="card-header pb-0 pt-3 ">
        <div class="float-start">
          <h5 class="mt-3 mb-0">Argon Configurator</h5>
          <p>See our dashboard options.</p>
        </div>
        <div class="float-end mt-4">
          <button class="btn btn-link text-dark p-0 fixed-plugin-close-button">
            <i class="fa fa-close"></i>
          </button>
        </div>
        <!-- End Toggle Button -->
      </div>
      <hr class="horizontal dark my-1">
      <div class="card-body pt-sm-3 pt-0 overflow-auto">
        <!-- Sidebar Backgrounds -->
        <div>
          <h6 class="mb-0">Sidebar Colors</h6>
        </div>
        <a href="javascript:void(0)" class="switch-trigger background-color">
          <div class="badge-colors my-2 text-start">
            <span class="badge filter bg-gradient-primary active" data-color="primary" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-dark" data-color="dark" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-info" data-color="info" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-success" data-color="success" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-warning" data-color="warning" onclick="sidebarColor(this)"></span>
            <span class="badge filter bg-gradient-danger" data-color="danger" onclick="sidebarColor(this)"></span>
          </div>
        </a>
        <!-- Sidenav Type -->
        <div class="mt-3">
          <h6 class="mb-0">Sidenav Type</h6>
          <p class="text-sm">Choose between 2 different sidenav types.</p>
        </div>
        <div class="d-flex">
          <button class="btn bg-gradient-primary w-100 px-3 mb-2 active me-2" data-class="bg-white" onclick="sidebarType(this)">White</button>
          <button class="btn bg-gradient-primary w-100 px-3 mb-2" data-class="bg-default" onclick="sidebarType(this)">Dark</button>
        </div>
        <p class="text-sm d-xl-none d-block mt-2">You can change the sidenav type just on desktop view.</p>
        <!-- Navbar Fixed -->
        <div class="d-flex my-3">
          <h6 class="mb-0">Navbar Fixed</h6>
          <div class="form-check form-switch ps-0 ms-auto my-auto">
            <input class="form-check-input mt-1 ms-auto" type="checkbox" id="navbarFixed" onclick="navbarFixed(this)">
          </div>
        </div>
        <hr class="horizontal dark my-sm-4">
        <div class="mt-2 mb-5 d-flex">
          <h6 class="mb-0">Light / Dark</h6>
          <div class="form-check form-switch ps-0 ms-auto my-auto">
            <input class="form-check-input mt-1 ms-auto" type="checkbox" id="dark-version" onclick="darkMode(this)">
          </div>
        </div>
        <a class="btn bg-gradient-dark w-100" href="https://www.creative-tim.com/product/argon-dashboard">Free Download</a>
        <a class="btn btn-outline-dark w-100" href="https://www.creative-tim.com/learning-lab/bootstrap/license/argon-dashboard">View documentation</a>
        <div class="w-100 text-center">
          <a class="github-button" href="https://github.com/creativetimofficial/argon-dashboard" data-icon="octicon-star" data-size="large" data-show-count="true" aria-label="Star creativetimofficial/argon-dashboard on GitHub">Star</a>
          <h6 class="mt-3">Thank you for sharing!</h6>
          <a href="https://twitter.com/intent/tweet?text=Check%20Argon%20Dashboard%20made%20by%20%40CreativeTim%20%23webdesign%20%23dashboard%20%23bootstrap5&amp;url=https%3A%2F%2Fwww.creative-tim.com%2Fproduct%2Fargon-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
            <i class="fab fa-twitter me-1" aria-hidden="true"></i> Tweet
          </a>
          <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.creative-tim.com/product/argon-dashboard" class="btn btn-dark mb-0 me-2" target="_blank">
            <i class="fab fa-facebook-square me-1" aria-hidden="true"></i> Share
          </a>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Add Title -->
  <div class="modal fade" id="modalAddTitle" role="dialog"  aria-labelledby="modalAddTitleLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalAddTitleLabel">Add Title Todo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="post" id="addTodo">
                <div class="form-group">
                  <label for="exampleInputEmail1">Title</label>
                  <input type="text" class="form-control" name="todo_title">
                </div>
              </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="addTitleTodo();">Save</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Add Todo -->
  <div class="modal fade" id="modalAddTodo" role="dialog"  aria-labelledby="modalAddTodoLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalAddTodoLabel">Add Todo</h5>
          <button type="button" class="close" onclick="closesModalAddNewTodo();">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="post" id="addTodo">
                <input type="hidden" id="title_id">
                <div class="form-group">
                  <label for="exampleInputEmail1">Todo Name</label>
                  <textarea name="" class="form-control" id="todo_name"></textarea>
                </div>
                <div class="form-group">
                  <label for="status_todo">Type Todo</label>
                  <select name="status_todo"  class="form-control" id="status_todo">
                    <option value="important">Important</option>
                    <option value="warning">Warning</option>
                    <option value="reminder">Reminder</option>
                    <option value="info">Info</option>
                  </select>
                </div>
                <div class="form-group">
                  <div class="container">
                    <div class="row">
                      <div class="col">
                        <label for="exampleInputEmail1">Date</label>
                        <input type="date" class="form-control" id="date_todo">
                      </div>
                      <div class="col">
                        <label for="exampleInputEmail1">Time Todo</label>
                        <input type="time" class="form-control" id="time_todo">
                      </div>
                    </div>
                    </div>
                </div>
              </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" onclick="closesModalAddNewTodo();">Close</button>
          <button type="button" class="btn btn-primary" onclick="createNewTodo();">Save</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal Update Title -->
  <div class="modal fade" id="modalUpdateTitle" role="dialog"  aria-labelledby="modalUpdateTitleLabel">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalUpdateTitleLabel">Update Title</h5>
          <button type="button" class="close" data-dismiss="modal" onclick="closeModalUpdateTitle();" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
              <form method="post" id="updateTodoTile">
                <input type="hidden" id="code_id">
                <div class="form-group">
                  <label for="exampleInputEmail1">Todo Name</label>
                  <input name="" class="form-control" id="inputUpdateTitle">
                </div>
              </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" onclick="closeModalUpdateTitle();" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="saveUpdateTitle();">Save</button>
        </div>
      </div>
    </div>
  </div>

  <!--   Core JS Files   -->
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
  <script src="assets/js/core/popper.min.js"></script>
  <script src="assets/js/core/bootstrap.min.js"></script>
  <script src="assets/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="assets/js/plugins/smooth-scrollbar.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <!-- <script src="js/fetch.js"></script> -->
  <script>
  // Swal.fire({
//         title: 'Are you sure?',
//         text: "You won't be able to revert this!",
//         icon: 'success',
//         showCancelButton: false,
//         showConfirmButton: false,
//         confirmButtonColor: '#3085d6',
//         timer: 2000,
//       })
const endpoint = "http://localhost:3000";
const getAllTitle = async () => {
$("#collapseOne").html("");
await fetch(`${endpoint}/v1/title-todo/data/all`)
        .then(response => response.json())
        .then(data => {
        let dataJson = data.data;
        dataJson.forEach(val => {
                let template = `<li class="nav-item pl-2 d-flex align-items-center justify-content-arround position-relative">
                                    <div class="dropdown">
                                        <button class="btn btn-link text-secondary mb-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v text-xs" aria-hidden="true"></i>
                                        </button>
                                        <div class="dropdown-menu" style="position: absolute;top: 0px!important;" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="javascript:void(0)" onclick="callUpdateModal('${val.id}','${val.todo_title}');">Edit</a>
                                            <a class="dropdown-item" href="javascript:void(0)" onclick="deleteTitleTodo('${val.id}','${val.todo_title}');">Delete</a>
                                        </div>
                                    </div>
                                    <a class="nav-link" href="javascript:void(0);" onclick="getTodoByTitleId(this, '${val.id}','${val.todo_title}')">
                                    <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                        <i class="ni ni-collection text-info text-sm opacity-10"></i>
                                    </div>
                                    <span class="nav-link-text ms-1 text-wrap">${val.todo_title}</span>
                                    </a>
                                </li>`
                $("#collapseOne").append(template)
        });
        })
        .catch(err =>  {
        console.log(err);
        Swal.fire({
            title: 'Message',
            text: `Connection refused!: ${err}`,
            icon: 'error',
            showCancelButton: false,
            showConfirmButton: false,
            confirmButtonColor: '#3085d6',
            timer: 2000,
        })
        });
}

getAllTitle();
const callUpdateModal = (id, title) => {
    $("#modalUpdateTitle").modal("show")
    $("#inputUpdateTitle").val(title)
    $("#code_id").val(id);
}


const deleteTitleTodo = (id, title) => {
    Swal.fire({
        title: 'Message',
        text: `Are u sure to delete todo '${title}''`,
        icon: 'warning',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonColor: '#3085d6',
      }).then( val => {
        if(val.isConfirmed) {
            let url = `${endpoint}/v1/title-todo/data/${id}`
            $.ajax({
                url: url,
                dataType: "JSON",
                type: "DELETE",
                async: true,
                data: {},
                success: function (data) {
                    if(data.status) {
                        Swal.fire({
                                title: 'Message:',
                                text: data.message,
                                icon: 'success',
                                showCancelButton: false,
                                showConfirmButton: false,
                                confirmButtonColor: '#3085d6',
                                timer: 2000,
                        });
                        getAllTitle();
                        } else {
                            Swal.fire({
                                title: 'Message:',
                                text: data.message,
                                icon: 'error',
                                showCancelButton: false,
                                showConfirmButton: false,
                                confirmButtonColor: '#3085d6',
                                timer: 2000,
                            })
                        }
                },
                error: function (xhr, exception) {
                    alert("Something error....");
                }
            }); 
        }
      });
}

const addTitleTodo = () => {
    let todoTitle = $("[name='todo_title']").val();
    let url = `${endpoint}/v1/title-todo/data`
    $.ajax({
        url: url,
        dataType: "JSON",
        type: "POST",
        async: true,
        data: JSON.stringify( {
            todo_title: todoTitle
        }),
        success: function (data) {
            if(data.status) {
            Swal.fire({
                    title: 'Message:',
                    text: data.message,
                    icon: 'success',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    timer: 2000,
            })
            getAllTitle();
            $("[name='todo_title']").val("");
            $("#modalAddTitle").hide()
            $('.modal-backdrop').remove();
            } else {
                Swal.fire({
                    title: 'Message:',
                    text: data.message,
                    icon: 'error',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    timer: 2000,
                })
            }
        },
        error: function (xhr, exception) {
            alert("Something error....");
        }
    }); 
}

const getAllTodoById = async (titleId, titleName, template = true) => { 
      let result = null;
      let liElement = "";
      await fetch(`${endpoint}/v1/todolist/data/${titleId}/all`)
            .then(response => response.json())
            .then(data => {
             result = data;
             let datas = data.data;
             if(data.status) {
               datas.forEach(val => {
                 let btnStyle = '';
                 if(val.status == "important") {
                  btnStyle = 'btn-danger';
                 } else if(val.status == "warning") {
                  btnStyle = 'btn-warning';
                 } else if(val.status == "reminder") {
                  btnStyle = 'btn-info';
                 } else {
                  btnStyle = 'btn-primary';
                 }
                 liElement += `<li class="list-group-item border-0 d-flex p-4 mb-2 glass border-radius-lg">
                               <div class="d-flex flex-column">
                                 <h4 class="mb-3 t-primary">${val.todo_name}</h4>
                                 <span class="mb-2 text-xs">Status: <span class="text-white rounded p-2 ${btnStyle} font-weight-bold ms-sm-2">${val.status}</span></span>
                                 <span class="mb-2 text-xs">Date: <span class="text-dark ms-sm-2 font-weight-bold">${moment(val.deadline).format("LLLL")}</span></span>
                                 <span class="text-xs">Created At: <span class="text-dark ms-sm-2 font-weight-bold">${moment(val.created_at).format("LLLL")}</span></span>
                               </div>
                               <div class="ms-auto text-end">
                                 <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="javascript:;"><i class="far fa-trash-alt me-2" aria-hidden="true"></i>Delete</a>
                                 <a class="btn btn-link text-dark px-3 mb-0" href="javascript:;"><i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                               </div>
                             </li>`;
               });
             } else {
               liElement += ` <li class="list-group-item border-0 d-flex p-4 mb-2 glass border-radius-lg">
                                <div class="d-flex flex-column w-100">
                                  <h4 class="mb-3 t-primary w-100 text-center font-italic">Todo doesnt exist</h4>
                                </div>
                            </li>`
             }
        });
  if(template) {
    let templateTodo = `<div class="container-fluid py-4 vh-95">
                        <div class="row hp-100">
                            <div class="col-md-12 mt-4 hp-100">
                              <div class="card hp-100" id="container-card-todo">
                                <div class="card-header pb-0 px-3 d-flex justify-content-between align-items-center">
                                  <h6 class="my-2">Todolist: ${titleName}</h6>
                                  <button class="btn btn-outline-primary btn-sm mb-2" onclick="modalAddNewTodo('${titleId}');">Add New Todo</button>
                                </div>
                                <span class="ml-2 text-center" id='countTodo'>Total Todo : ${result.status ? result.total_data : 0}</span>
                                <div class="card-body pt-4 p-3 hp-100 bg-gradient">
                                  <ul class="list-group overflow-auto hp-100" id='containerUl'>
                                    ${liElement}
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                          <footer class="footer pt-4 mt-2">
                            <div class="container-fluid">
                              <div class="row align-items-center justify-content-lg-between">
                                <div class="col-lg-12 mb-lg-0 mb-4 text-center">
                                  <div class="copyright text-center text-sm text-muted text-lg-start">
                                    made with <i class="fa fa-heart"></i> by
                                    <a href="https://www.creative-tim.com" class="font-weight-bold" target="_blank">Azis Zuhri Pratomo</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </footer>
                        </div>
                      </div>`
      $("#containerTodo").append(templateTodo);
  }
    return result;
}

const saveUpdateTitle = () => {
    let dataTitle = $("#inputUpdateTitle").val();
    let id =  $("#code_id").val();
    console.log(id);
    let url = `${endpoint}/v1/title-todo/data/${parseInt(id)}`
    $.ajax({
        url: url,
        dataType: "JSON",
        type: "PUT",
        async: true,
        data: JSON.stringify( {
            todo_title: dataTitle
        }),
        success: function (data) {
            if(data.status) {
            Swal.fire({
                    title: 'Message:',
                    text: data.message,
                    icon: 'success',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    timer: 2000,
            })
            getAllTitle();
            closeModalUpdateTitle();
            $('.modal-backdrop').remove();
            } else {
                Swal.fire({
                    title: 'Message:',
                    text: data.message,
                    icon: 'error',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    timer: 2000,
                })
            }
        },
        error: function (xhr, exception) {
            alert("Something error....");
        }
    }); 
}

const getTodoByTitleId = async (self, code_id, titleName) => {
  $("body *").removeClass("link-active")
  $(self).addClass("link-active")
  $("#containerTodo").html("");
  let dataJson = await getAllTodoById(code_id, titleName);
  $("#containerTodo").hide();
  $("#containerTodo").delay(300).show(300);
}

const modalAddNewTodo = (code_id) => {
  $("#title_id").val(code_id);
  $("#modalAddTodo").modal("show");
}

const closesModalAddNewTodo = () => {
  $("#modalAddTodo").modal("hide");
}

const createNewTodo = async () => {
  let titleId = parseInt($("#title_id").val());
  let todoName = $("#todo_name").val();
  let status = $("#status_todo").val();
  let deadline = `${$("#date_todo").val()} ${$("#time_todo").val()}:00`
  let url = `${endpoint}/v1/todolist/data`
  $.ajax({
        url: url,
        dataType: "JSON",
        type: "POST",
        async: true,
        data: JSON.stringify( {
            title_id: titleId,
            todo_name: todoName,
            deadline,
            status
        }),
        success: async function (data) {
            if(data.status) {
            Swal.fire({
                    title: 'Message:',
                    text: data.message,
                    icon: 'success',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    timer: 2000,
            })
            let dataJson = await getAllTodoById(data.data.title_id, data.data.title_name, false);
            let countOfData = dataJson.total_data;
            dataJson = dataJson.data;
            let elTemp = '';
            dataJson.forEach(val => {
              let btnStyle = '';
                 if(val.status == "important") {
                  btnStyle = 'btn-danger';
                 } else if(val.status == "warning") {
                  btnStyle = 'btn-warning';
                 } else if(val.status == "reminder") {
                  btnStyle = 'btn-info';
                 } else {
                  btnStyle = 'btn-primary';
                 }
              elTemp += `<li class="list-group-item border-0 d-flex p-4 mb-2 glass border-radius-lg">
                            <div class="d-flex flex-column">
                              <h4 class="mb-3 t-primary">${val.todo_name}</h4>
                              <span class="mb-2 text-xs">Status: <span class="text-white rounded p-2 ${btnStyle} font-weight-bold ms-sm-2">${val.status}</span></span>
                              <span class="mb-2 text-xs">Date: <span class="text-dark ms-sm-2 font-weight-bold">${moment(val.deadline).format("LLLL")}</span></span>
                              <span class="text-xs">Created At: <span class="text-dark ms-sm-2 font-weight-bold">${moment(val.created_at).format("LLLL")}</span></span>
                            </div>
                            <div class="ms-auto text-end">
                              <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="javascript:;"><i class="far fa-trash-alt me-2" aria-hidden="true"></i>Delete</a>
                              <a class="btn btn-link text-dark px-3 mb-0" href="javascript:;"><i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                            </div>
                          </li>`;
            });
            $("#containerUl").html("");
            $("#countTodo").text(`Total Todo : ${countOfData}`);
            $("#containerUl").append(elTemp);
            $("#containerUl").hide();
            $("#containerUl").delay(300).show(300);
            $('.modal-backdrop').remove();
            closesModalAddNewTodo();
            } else {
                Swal.fire({
                    title: 'Message:',
                    text: data.message,
                    icon: 'error',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    timer: 2000,
                })
            }
        },
        error: function (xhr, exception) {
            alert("Something error....");
        }
  }); 
}

const closeModalUpdateTitle = () => {
    $("#modalUpdateTitle").modal("hide");
}
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="assets/js/argon-dashboard.min.js?v=2.0.4"></script>
</body>
<!-- <div class="container">
  <div class="row">
    <div class="col">
      <button class="btn btn-danger">Delete</button>
    </div>
    <div class="col">
      <button class="btn btn-danger">Edit</button>
    </div>
  </div>
</div> -->
</html>