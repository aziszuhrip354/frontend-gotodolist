// Swal.fire({
//         title: 'Are you sure?',
//         text: "You won't be able to revert this!",
//         icon: 'success',
//         showCancelButton: false,
//         showConfirmButton: false,
//         confirmButtonColor: '#3085d6',
//         timer: 2000,
//       })
const endpoint = "http://localhost:3000";
const getAllTitle = async () => {
$("#collapseOne").html("");
await fetch(`${endpoint}/v1/title-todo/data/all`)
        .then(response => response.json())
        .then(data => {
        let dataJson = data.data;
        dataJson.forEach(val => {
                let template = `<li class="nav-item pl-2 d-flex align-items-center justify-content-arround position-relative">
                                    <div class="dropdown">
                                        <button class="btn btn-link text-secondary mb-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v text-xs" aria-hidden="true"></i>
                                        </button>
                                        <div class="dropdown-menu" style="position: absolute;top: 0px!important;" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="javascript:void(0)" onclick="callUpdateModal('${val.id}','${val.todo_title}');">Edit</a>
                                            <a class="dropdown-item" href="javascript:void(0)" onclick="deleteTitleTodo('${val.id}','${val.todo_title}');">Delete</a>
                                        </div>
                                    </div>
                                    <a class="nav-link " href="#">
                                    <div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                                        <i class="ni ni-collection text-info text-sm opacity-10"></i>
                                    </div>
                                    <span class="nav-link-text ms-1 text-wrap">${val.todo_title}</span>
                                    </a>
                                </li>`
                $("#collapseOne").append(template)
        });
        })
        .catch(err =>  {
        console.log(err);
        Swal.fire({
            title: 'Message',
            text: `Connection refused!: ${err}`,
            icon: 'error',
            showCancelButton: false,
            showConfirmButton: false,
            confirmButtonColor: '#3085d6',
            timer: 2000,
        })
        });
}

getAllTitle();
const callUpdateModal = (id, title) => {
    $("#modalUpdateTitle").modal("show")
    $("#inputUpdateTitle").val(title)
    $("#code_id").val(id);
}


const deleteTitleTodo = (id, title) => {
    Swal.fire({
        title: 'Message',
        text: `Are u sure to delete todo '${title}''`,
        icon: 'warning',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonColor: '#3085d6',
      }).then( val => {
        if(val.isConfirmed) {
            let url = `${endpoint}/v1/title-todo/data/${id}`
            $.ajax({
                url: url,
                dataType: "JSON",
                type: "DELETE",
                async: true,
                data: {},
                success: function (data) {
                    if(data.status) {
                        Swal.fire({
                                title: 'Message:',
                                text: data.message,
                                icon: 'success',
                                showCancelButton: false,
                                showConfirmButton: false,
                                confirmButtonColor: '#3085d6',
                                timer: 2000,
                        });
                        getAllTitle();
                        } else {
                            Swal.fire({
                                title: 'Message:',
                                text: data.message,
                                icon: 'error',
                                showCancelButton: false,
                                showConfirmButton: false,
                                confirmButtonColor: '#3085d6',
                                timer: 2000,
                            })
                        }
                },
                error: function (xhr, exception) {
                    alert("Something error....");
                }
            }); 
        }
      });
}

const addTitleTodo = () => {
    let todoTitle = $("[name='todo_title']").val();
    let url = `${endpoint}/v1/title-todo/data`
    $.ajax({
        url: url,
        dataType: "JSON",
        type: "POST",
        async: true,
        data: JSON.stringify( {
            todo_title: todoTitle
        }),
        success: function (data) {
            if(data.status) {
            Swal.fire({
                    title: 'Message:',
                    text: data.message,
                    icon: 'success',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    timer: 2000,
            })
            getAllTitle();
            $("[name='todo_title']").val("");
            $("#modalAddTitle").hide()
            $('.modal-backdrop').remove();
            } else {
                Swal.fire({
                    title: 'Message:',
                    text: data.message,
                    icon: 'error',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    timer: 2000,
                })
            }
        },
        error: function (xhr, exception) {
            alert("Something error....");
        }
    }); 
}

const saveUpdateTitle = () => {
    let dataTitle = $("#inputUpdateTitle").val();
    let url = `${endpoint}/v1/title-todo/data`
    $.ajax({
        url: url,
        dataType: "JSON",
        type: "PUT",
        async: true,
        data: JSON.stringify( {
            todo_title: dataTitle
        }),
        success: function (data) {
            if(data.status) {
            Swal.fire({
                    title: 'Message:',
                    text: data.message,
                    icon: 'success',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    timer: 2000,
            })
            getAllTitle();
            closeModalUpdateTitle();
            $('.modal-backdrop').remove();
            } else {
                Swal.fire({
                    title: 'Message:',
                    text: data.message,
                    icon: 'error',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonColor: '#3085d6',
                    timer: 2000,
                })
            }
        },
        error: function (xhr, exception) {
            alert("Something error....");
        }
    }); 
}
